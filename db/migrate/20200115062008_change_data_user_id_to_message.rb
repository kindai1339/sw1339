class ChangeDataUserIdToMessage < ActiveRecord::Migration[5.0]
  def change
    change_column :messages, :user_id, :string
  end
end
