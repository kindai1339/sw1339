Rails.application.routes.draw do
  get 'rooms/show'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'top#index'
  get 'top/main'
  get 'top/login'
  get 'user/index'
  get 'user/mypage'
  post 'top/login'
  post 'user', to: 'user#create'
  post 'top/login'
  post 'top/main'
  get  'top/logout'
  get 'likes/create'
  
  resources :users do
    member do
      get :liking, :likers
    end
  end
  resources :user
  resources :chat
  resources :rooms
  resources :likes
  resources :relationships,       only: [:create, :destroy]
  mount ActionCable.server => '/cable'
end

