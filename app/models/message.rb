class Message < ApplicationRecord

  # createの後にコミットする { MessageBroadcastJobのperformを遅延実行 引数はself }
  after_create_commit { MessageBroadcastJob.perform_later self }

  belongs_to :room, optional: true
  belongs_to :user, optional: true
end
