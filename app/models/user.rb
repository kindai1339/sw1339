class User < ApplicationRecord
  has_many :messages
    
  has_many :active_relationships, class_name:  "Relationship",
                                  foreign_key: "liker_id",
                                  dependent:   :destroy
  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "liked_id",
                                   dependent:   :destroy
  has_many :liking, through: :active_relationships, source: :liked
  has_many :likers, through: :passive_relationships, source: :liker
  # ユーザーをフォローする
  def like(other_user)
    active_relationships.create(liked_id: other_user.id)
  end

  # ユーザーをアンフォローする
  def unlike(other_user)
    active_relationships.find_by(liked_id: other_user.id).destroy
  end

  # 現在のユーザーがフォローしてたらtrueを返す
  def liking?(other_user)
    liking.include?(other_user)
  end

  #  has_many :like_users,source: :user
  #  has_many :likes
    
  #def like(user)
  #  likes.create(user_id: user.id)
  #end
  
  #def unlike(user)
  #  likes.find_by(user_id: user.id).destroy
  #end
  
  #def liked?(user)
  #  like_users.include?(user)
  #end
end
