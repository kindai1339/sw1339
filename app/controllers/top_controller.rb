class TopController < ApplicationController

 def index
    @rooms = Room.all.order(:id)
    @users = User.all
 end

 def main
    @rooms = Room.all.order(:id)
    @users = User.all
      if session[:login_uid] != nil
        flash[:notice] = "ログインに成功しました"
        render'top/main'    
      else
        session[:login_uid] = nil
        flash[:notice] = "ログインに失敗しました"
        render top_main_path
      end
 end
  
 def login
    @users = User.all
    @rooms = Room.all.order(:id)
    if user = User.find_by_uid(params[:uid])
        if params[:uid] == user.uid && BCrypt::Password.new(user.pass) == params[:pass]
          session[:login_uid] = user.uid
          session[:login_name]=user.name
          session[:login_id]=user.id
          render top_main_path
        else
          flash[:notice] = "IDかパスワードが違います"
          render top_login_path
        end
    end
 end
 
 def logout
  session[:login_uid] = nil
  redirect_to '/'
 end

 def mypage
   user = User.find_by_uid(session[:login_uid])
   session[:login_name]=user.name
 end

end
