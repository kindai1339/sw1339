class RelationshipsController < ApplicationController
  def create
    user = User.find(params[:id])
    current_user = User.find_by(uid: session[:login_uid])
    current_user.like(user)
    redirect_to top_main_path
  end

  def destroy
    user = Relationship.find_by(liked_id: params[:id]).liked
    current_user = User.find_by(uid: session[:login_uid])
    current_user.unlike(user)
    redirect_to top_main_path
  end
end
