class RoomsController < ApplicationController
  
  def index
    @users = User.all
    @rooms = Room.all.order(:id)
  end


  def new
    @users = User.all
    @room = Room.new
    @rooms = Room.all.order(:id)
    render "new"
  end

  def create
      title = params[:room][:title]
      comment = params[:room][:comment]
      @room = Room.new(title: title,comment: comment)
      @rooms = Room.all.order(:id)
      @users = User.all
    if @room.save
      flash[:info] = 'ルームを作成しました。'
      render top_main_path
    else
      render new_room_path
    end
  end
  
  def show
    # メッセージ一覧を取得
    @rooms = Room.all.order(:id)
    @room = Room.find(params[:id])
    @users = User.all
    @messages = @room.messages
    user = User.find_by_uid(session[:login_uid])
    session[:login_name]=user.name
  end
end
