class LikesController < ApplicationController

  def index
    @rooms = Room.all.order(:id)
    @users = User.all
  end

  def create
    userd = User.find(params[:user_id])
    user = User.find_by(uid: session[:login_uid])
    unless userd.liking?(user)
     userd.like(user)
    end
    redirect_to top_main_path
  end
  
  def destroy
    userd = User.find(params[:id])
    user = User.find_by(uid: session[:login_uid])
    userd.unlike(user)
    redirect_to root_path
  end
end
