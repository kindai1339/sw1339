class UserController < ApplicationController
  def index
    @users = User.all
    @rooms = Room.all.order(:id)
  end

  def new
    @user = User.new
    render "new"
  end

  def create
      uid = params[:user][:uid]
      name = params[:user][:name]
      pass = params[:user][:pass]
      text = params[:user][:text]
      signup_password = BCrypt::Password.create(pass)
      @user = User.new(uid: uid,name: name,pass: signup_password,text: text)
    if @user.save
      flash[:info] = 'ユーザを登録完了しました。'
      redirect_to '/'
    else
      render new_user_path
    end
  end
  
  def show
    @user = User.find(params[:id])
  end

  def destroy
    user = User.find(params[:id])
    user.destroy
    redirect_to users_path
  end
  def update
    user = User.find_by(uid: session[:login_uid])
    user.name = params[:user][:name]
    user.text = params[:user][:text]
    user.save
    render '/user/index'
  end
  def mypage
    @user = User.find_by(uid: session[:login_uid])
    session[:text] = @user.text
    session[:name] = @user.name
  end

end
